package ictgradschool.industry.lab09;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Test1 {

    private String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};


    private void start(){
       arrayList();
       arrayIterator();
       arrayEnhanced();
    }

    public List<String> toArrayList(){
        List<String> arrayList = new ArrayList<>();
        for (int i = 0; i<array.length; i++){
            arrayList.add(array[i]);
        }
        return arrayList;
    }

    public static void arrayList(){
        List<String> arrayList = new ArrayList<String>(Arrays.asList("ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"));
        for (int i = 0; i< arrayList.size(); i++){
            arrayList.set(i, arrayList.get(i).toLowerCase());
        }
    }


    public void arrayEnhanced(){
        List<String> arrayList = toArrayList();
        List<String> newarrayList = new ArrayList<String>();
        for (String num : arrayList){
            newarrayList.add(num.toLowerCase());
        }
        arrayList = newarrayList;

        for (int i = 0; i < arrayList.size(); i++){
            System.out.println(arrayList.get(i));
        }
    }

    public void arrayIterator(){
        List<String> arrayList = toArrayList();
        Iterator<String> iterator = arrayList.iterator();
        List<String> newArray = new ArrayList<String>();
        while (iterator.hasNext()){
            String element = iterator.next().toLowerCase();
            newArray.add(element);
        }
    }


//q1 a)  "not funny" compares a character and a string - different.
//q1 b) The answer will be same object.  "pt2" points to the same object as pt1.
//q1 c) no element with index 1.

    public static void main(String[] args) {
        Test1 ex = new Test1();
        ex.start();
    }
}
