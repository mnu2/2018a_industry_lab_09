package ictgradschool.industry.lab09.bonus.ex01;

import java.lang.reflect.Type;

/**
 * Classes implementing this interface are capable of testing an object to see if some condition is true.
 */
public interface IPredicate<Type> {

    /**
     * Tests the given object to see if some condition is true.
     *
     * @param type the object to test
     * @return true if the condition is met, false otherwise.
     */
    public boolean test(Type type);

}
